import model.Conexao;
import model.Funcionario;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Criando objeto");

        //Criando um objeto e salvando no banco
        Funcionario f = new Funcionario();
        f.setId(1);
        f.setNome("Fulano de Tall");
        f.create();

        f.setNome("nome 2");
        f.update();

        
        System.out.println(Funcionario.getAll());
    }
}
