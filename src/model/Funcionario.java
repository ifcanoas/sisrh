package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;


import java.sql.SQLException;
import java.util.ArrayList;


public class Funcionario {
    private int id;
    private String nome;
    private double salario;
    
    
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public double getSalario() {
        return salario;
    }
    public void setSalario(double salario) {
        this.salario = salario;
    }


    public void create(){
        Conexao c = new Conexao();
        Connection dbConn = c.getConexao();

        String sql = "INSERT INTO funcionario (id, nome) VALUES (?,?)";

        try {
            PreparedStatement  ps = dbConn.prepareStatement(sql);

            ps.setInt(1, this.id);
            ps.setString(2, this.nome);
            ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void update(){
        Conexao c = new Conexao();
        Connection dbConn = c.getConexao();

        String sql = "UPDATE funcionario SET nome = ? WHERE id = ?";
        
        try {
            PreparedStatement ps = dbConn.prepareStatement(sql);
        
            ps.setString(1, this.nome);
            ps.setInt(2, this.id);
            ps.executeUpdate();
        
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private static ArrayList<Funcionario> getList(String sql){
        ArrayList<Funcionario> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection db = c.getConexao();

        try {
            Statement st = db.createStatement();

            ResultSet rs = st.executeQuery(sql);

            while(rs.next()){
                Funcionario f = new Funcionario();
                f.setId(rs.getInt("id"));
                f.setNome(rs.getString("nome"));
                f.setSalario(rs.getDouble("salario"));
                lista.add(f);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } 
        return lista;
    }

    //Não Fazer repensar 
    public static Funcionario getOneById(int id){
        String sql = "SELECT * FROM funcionario WHERE id = "+ id;

        return Funcionario.getList(sql).get(0);
    }

    public static ArrayList<Funcionario> getAll(){
        
        String sql = "SELECT * FROM funcionario";

        return Funcionario.getList(sql);
    }

    public static ArrayList<Funcionario> getAllByImpostoDeRenda(){
        
        String sql = "SELECT * FROM funcionario WHERE salario > 1200";
        return Funcionario.getList(sql);

    }


    @Override
    public String toString() {
        return " [id=" + id + ", nome=" + nome + "]";
    }

    
    
}
