package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Setor {
    private int id;
    private String nome;
    private String funcao;
    private Funcionario gerente;

    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getFuncao() {
        return funcao;
    }
    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }
    public Funcionario getGerente() {
        return gerente;
    }
    public void setGerente(Funcionario gerente) {
        this.gerente = gerente;
    }

     public void create(){
        Conexao c = new Conexao();
        Connection dbConn = c.getConexao();

        String sql = "INSERT INTO setor ( nome, funcionario_id) VALUES (?,?)";

        try {
            PreparedStatement  ps = dbConn.prepareStatement(sql);

            //Por ter autoincrement não preciso de id
            ps.setString(1, this.nome);
            ps.setInt(2, this.gerente.getId());
            ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    
}
