package model;

import java.time.LocalTime;

public class Salario {
    private Integer id;
    private Integer funcionaroId;
    private Double salarioBruto;
    private Double inss;
    private Double impostoRenda;
    private LocalTime dataSalario;
  }